require('dotenv').config()

const bodyParser = require('body-parser')
const express = require('express')
const jwt = require('jsonwebtoken')

const app = express()
app.use(express.json())

const posts = [
    {
        username: "john",
        email : "iamjohn@email.com",
        DOB : "1-1-2000",
        title: "you are viewing posts of john",
        role : "user"
    },
    {
        username: "jane",
        email : "iamjane@email.com",
        DOB : "12-12-2012",
        title: "you are viewing posts of jane",
        role : "user"
    }
]

const admin = [
    {
        username : "karan",
        email : "iamkaran@email.com",
        DOB : "1-2-2003",
        title : "you are now on admin page",
        role : "admin"
    }
]

app.get('/posts', authenticateToken, (req, res) => {
    res.json(posts.filter(post => post.username === req.user.name))
})

app.get('/admin', authenticateToken, (req, res) => {
    res.json(admin)
})

app.post('/login', (req, res) => {
    //Authenticate user
    const username = req.body.username
    const user = {name : username}

    const accessToken = jwt.sign(user,process.env.SECRET,{algorithm : "none"})
    res.json({accessToken: accessToken})
})

function authenticateToken(req, res, next){
    const authHeader = req.headers['authorization']
    const token = authHeader && authHeader.split(' ')[1]
    if(token == null) return res.sendStatus(401)

    jwt.verify(token, process.env.SECRET,{algorithms : ["none"]}, (err, user) => {
        if(err) return res.sendStatus(403)
        req.user = user
        next()
    })
}

app.listen(8000)